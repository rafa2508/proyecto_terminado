<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::all();

        return Inertia::render('Blogs/Index', [
            'blogs' => $blogs
        ]);
    }

    public function create()
    {
        return Inertia::render('Blogs/Create');
    }

    public function uploadImage ()
    {

    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $blogs = new Blog();
        $blogs->title = $request->title;
        $blogs->description = $request->description;
        $blogs->info = $request->info;
        $blogs->save();
        DB::commit();

        return redirect('/blogs');
    }

    public function show($id)
    {
        $blogs = Blog::findOrFail($id);

        return Inertia::render('Blogs/Show', [
            'blogs' => $blogs,
        ]);
    }

    public function edit($id)
    {
        $blogs = Blog::findOrFail($id);

        return Inertia::render('Blogs/Edit', [
            'blogs' => $blogs,
        ]);
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $blogs = Blog::findOrFail($id);
        $blogs->title = $request->title;
        $blogs->description = $request->description;
        $blogs->info = $request->info;
        $blogs->save();
        DB::commit();

        return redirect('/blogs');
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        $blogs = Blog::findOrFail($id);
        $blogs->delete();
        DB::commit();

        return back();
    }
}
